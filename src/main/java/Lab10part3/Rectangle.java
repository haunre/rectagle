/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Lab10part3;

/**
 *
 * @author 123
 */
public class Rectangle {
    private double height;
    private double width;

    public Rectangle(double height, double width) {
        this.height = height;
        this.width = width;
    }

    public double getHeight() {
        return height;
    }

    public void setHeight(double height) {
        this.height = height;
    }

    public double getWidth() {
        return width;
    }

    public void setWidth(double width) {
        this.width = width;
    }
    
    public double getArea(){
        return height * width;
    }
    
    public double getPerimiter(){
    return height + width  * 2;
    }
    
    public final static void setDimensions(Rectangle r, int w, int h){
            r.setWidth(w);
            r.setHeight(h);
            
        }

}
